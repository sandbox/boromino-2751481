<?php

/**
 * @file
 * Default rule configurations for Commerce product display limit module.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_product_display_limit_default_rules_configuration() {
  $config = array();

  $rules_add_to_cart_product_display_limit = '{ "rules_add_to_cart_product_display_limit" : {
      "LABEL" : "Add to cart product display limit",
      "PLUGIN" : "and",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Cart" ],
      "REQUIRES" : [ "commerce_product_display_limit" ],
      "USES VARIABLES" : { "line_item" : { "label" : "Line item", "type" : "commerce_line_item" } },
      "AND" : [
        { "commerce_product_display_limit_compare" : {
            "commerce_line_item" : [ "line-item" ],
            "product_display" : { "value" : { "" : "" } }
          }
        },
        { "commerce_product_display_limit_compare_quantity" : {
            "commerce_line_item" : [ "line-item" ],
            "commerce_order" : [ "site:current-cart-order" ],
            "operator" : "\u003E",
            "value" : "1"
          }
        }
      ]
    }
  }';
  $config['rules_add_to_cart_product_display_limit'] = rules_import($rules_add_to_cart_product_display_limit);

  return $config;
}
