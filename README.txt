INTRODUCTION
------------
The commerce product display limit module allows you to limit the product
quantity by product display.

EXAMPLE USE CASE
----------------
Selling tickets of different categories for an event, only 2 tickets per
customer allowed.

Create a product for each category and display them together in a product
display (node) for the event. Limit allowed ticket quantity a customer can
order, independent from what categories he buys.

INSTALLATION
------------
The module contains 2 submodules:
- Formatter settings (limit quantity on product display)
- Views (limit quantity on cart display edit form)

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

DEPENDENCIES
------------
- commerce_cart
- rules (Commerce product display limit only)
- views
- field_formatter_settings (Commerce Product Display Limit Formatter Settings only)

CONFIGURATION
-------------

Add to cart form
----------------
In Structure > Content types > Manage display (of the corresponding display)
(admin/structure/types/manage/<product-display>/display) limit the quantity
in the add to cart form settings by disabling the quantity form field.

This setting form is only displayed if the quantity widget is enabled on the
add to cart form.

Cart edit form
--------------
In Structure > Views > Shopping cart form > Commerce Line Item Quantity text
field (admin/structure/views/view/commerce_cart_form/edit) disable the quantity
field and choose the product displays to limit the quantity for.

This setting form is only displayed if the quantity field is not hidden.

Validate limitation
-------------------
The commerce product display limit main module adds a rules component to
validate the limitation.

In Configuration > Workflow > Rules > Components edit
'Add to cart product display limit'. Choose which product displays to limit the
quantity for in 'Product display comparison' condition and set the limit in
'Total product display quantity comparison' condition.

TODO
----
The validation only applies to the current cart. The order history is not
included, yet.

MAINTAINERS
-----------
Current maintainers:
 * Richard Papp (boromino) - https://drupal.org/user/859722
