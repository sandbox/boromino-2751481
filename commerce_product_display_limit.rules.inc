<?php

/**
 * @file
 * Events and reaction rules for commerce product display limit module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_product_display_limit_rules_condition_info() {
  $conditions = array();

  $conditions['commerce_product_display_limit_compare'] = array(
    'group' => t('Commerce Order'),
    'label' => t('Product display comparison'),
    'parameter' => array(
      'commerce_line_item' => array(
        'label' => t('Product line item'),
        'type' => 'commerce_line_item',
      ),
      'product_display' => array(
        'type' => 'list<text>',
        'label' => t('Product Display(s)'),
        'description' => t('The product display(s) to look for on the order.'),
        'options list' => 'commerce_product_display_limit_options_list',
        'restriction' => 'input',
        'optional' => FALSE,
      ),
    ),
  );
  
  $conditions['commerce_product_display_limit_compare_quantity'] = array(
    'group' => t('Commerce Order'),
    'label' => t('Total product display quantity comparison'),
    'parameter' => array(
      'commerce_line_item' => array(
        'label' => t('Product line item'),
        'type' => 'commerce_line_item',
      ),
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order whose product line item quantities should be totalled. If the specified order does not exist, the comparison will act as if it is against a quantity of 0.'),
      ),
      'operator' => array(
        'type' => 'text',
        'label' => t('Operator'),
        'description' => t('The comparison operator to use against the total number of product per display on the order.'),
        'default value' => '>',
        'options list' => 'commerce_numeric_comparison_operator_options_list',
        'restriction' => 'input',
      ),
      'value' => array(
        'type' => 'integer',
        'label' => t('Quantity'),
        'default value' => 1,
        'description' => t('The value to compare against the total quantity of products on the order.'),
      ),
    ),
  );

  return $conditions;
}

/**
 * Condition callback: compare product display of line item.
 *
 * @param object $line_item
 *   The line item to add to or update in cart.
 * @param array $product_type
 *   The product display names to compare.
 *
 * @return bool
 *   TRUE if line item is limited by display, FALSE otherwise.
 */
function commerce_product_display_limit_compare($line_item, array $product_type) {
  $display_id = $line_item->data['context']['entity']['entity_id'];
  
  if (commerce_product_display_limit_check_display($display_id, $product_type)) {
    return TRUE;
  }
  
  return FALSE;
}

/**
 * Condition callback: compare total order quantity by product display.
 *
 * @param object $order
 * @param string $op
 * @param double $value
 * @param double $quantity
 *
 * @return bool
 *   TRUE if the comparison applies, FALSE otherwise.
 */
function commerce_product_display_limit_compare_quantity($line_item, $order, $operator, $value) {
  $quantity = array();

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($order_wrapper->commerce_line_items as $delta => $order_item) {
    $order_item = $order_item->value();
    $entity_id = $order_item->data['context']['entity']['entity_id'];
    $quantity[$entity_id] = $order_item->quantity;
  }

  $line_item_display_id = $line_item->data['context']['entity']['entity_id'];
  // Add to cart
  if (!empty($line_item->is_new)) {
    if (array_key_exists($line_item_display_id, $quantity)) {
      $quantity[$line_item_display_id] += $line_item->values['quantity'];
    }
    else {
      $quantity[$line_item_display_id] = $line_item->values['quantity'];
    }
  }
  // Update cart
  else {
    $quantity[$line_item_display_id] = ($line_item->values['quantity'])
                                     ? $line_item->values['quantity']
                                     : 0;
  }

  if (commerce_product_display_limit_comparison($operator, $value, $quantity[$line_item_display_id])) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Callback function to create product display options list.
 *
 * @return array
 *   Product displays.
 */
function commerce_product_display_limit_options_list() {
  $product_displays = commerce_product_display_limit_get_product_displays();
  return array_merge(array('' => '<' . t('none') . '>'), $product_displays);
}

/**
 * Check if product display is limited.
 *
 * Retrieves a product display name by node id and checks if rule applies to it.
 *
 * @param int $display_id
 *   Id of the product display.
 * @param array $product_type
 *   Limited product display names.
 *
 * @return bool
 *   TRUE if product is limited by display, FALSE otherwise.  
 */
function commerce_product_display_limit_check_display($display_id, array $product_type) {
  $product_display = db_select('node', 'n')
                      ->fields('n', array('type'))
                      ->condition('nid', $display_id, '=')
                      ->execute()
                      ->fetchField();

  return in_array($product_display, $product_type);
}

/**
 * Compare values using different operators.
 *
 * @param string $operator
 *   Comparison operator.
 * @param int $value
 *   Value to compare to, set in the rules condition.
 * @param int $quantity
 *   Value to compare, supplied by user interaction.
 *
 * @return bool
 */
function commerce_product_display_limit_comparison($operator, $value, $quantity) {
  switch ($operator) {
    case '<':
      return $quantity < $value;
    case '<=':
      return $quantity <= $value;
    case '=':
      return $quantity == $value;
    case '>=':
      return $quantity >= $value;
    case '>':
      return $quantity > $value;
    default:
      return FALSE;
  }
}
