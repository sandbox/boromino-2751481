<?php

/**
 * @file
 * Field handler to disable the quantity form field of a line item. It
 * extends the commerce line item handler to add the corresponding option form.
 */

/**
 * Field handler to disable the field to change quantity of a line item.
 */
class commerce_product_display_limit_views_handler_field_edit_quantity extends commerce_line_item_handler_field_edit_quantity {

  function option_definition() {
    $options = parent::option_definition();
    
    $options['disable'] = array('default' => isset($this->definition['disable']) ? $this->definition['disable'] : FALSE, 'bool' => TRUE);
    $options['product_displays'] = array('default' => isset($this->definition['product_displays']) ? $this->definition['product_displays'] : array());
    
    return $options;
  }
  
  /**
   * Disable quantity field.
   */
  function options_form(&$form, &$form_state) {
    $form['disable'] = array(
      '#title' => t('Disable'),
      '#description' => t('Display the field but do not allow user to edit quantity.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['disable']),
      '#dependency' => array(
        'edit-options-exclude' => array(0),
      ),
    );
    
    $options = (module_load_include('module', 'commerce_product_display_limit'))
             ? commerce_product_display_limit_get_product_displays()
             : array();
    $form['product_displays'] = array(
      '#title' => t('Product displays'),
      '#options' => $options,
      '#type' => 'select',
      '#multiple' => TRUE,
      '#default_value' => $this->options['product_displays'],
      '#description' => t('Choose the product displays to disable the quantity field for.'),
      '#dependency' => array(
        'edit-options-exclude' => array(0),
        'edit-options-disable' => array(1),
      ),
      '#dependency_count' => 2,
    );

    parent::options_form($form, $form_state);
  }
  
  /**
   * Returns the form which replaces the placeholder from render().
   */
  function views_form(&$form, &$form_state) {
    parent::views_form($form, $form_state);

    if (!empty($this->options['disable'])) {
      $product_displays = array();
      foreach ($this->view->result as $row_id => $row) {
        $data = $row->_field_data['commerce_line_item_field_data_commerce_line_items_line_item_']['entity']->data;
        $product_display_id = $data['context']['entity']['entity_id'];
        if (!array_key_exists($product_display_id, $product_displays)) {
          $entity = entity_load_single($data['context']['entity']['entity_type'], $product_display_id);
          $product_displays[$product_display_id] = $entity->type;
        }
        if (in_array($product_displays[$product_display_id], $this->options['product_displays'])) {
          $form[$this->options['id']][$row_id]['#disabled'] = TRUE;
        }
      }
    }
  }
}