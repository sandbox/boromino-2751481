<?php

/**
 * @file
 * Extend views handler to disable quantity field.
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_product_display_limit_views_views_data_alter(&$data) {
  $data['commerce_line_item']['edit_quantity']['field']['handler'] = 'commerce_product_display_limit_views_handler_field_edit_quantity';
}